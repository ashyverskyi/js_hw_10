/* 

Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
   Створення: document.createElement(tagName), innerHTML або outerHTML, document.createDocumentFragment(), insertAdjacentHTML() або insertAdjacentElement()
   Додавання: before, after, prepend, append або beforebegin, afterbegin, beforeend, afterend
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
   const navEl = document.getElementsByClassName("navigation");
   const removeFirst = navEl[0];
   removeFirst.remove();
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
   before, after, prepend, append або beforebegin, afterbegin, beforeend, afterend


Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

 */

// TASK 1

const newElement = document.createElement('a');
newElement.textContent = "Learn More";
newElement.setAttribute('href', '#');
const footerElement = document.getElementsByTagName('footer')[0];
const paragraphElement = footerElement.querySelector('p');
footerElement.insertBefore(newElement, paragraphElement.nextSibling);

// TASK 2

const selectElement = document.createElement('select');
selectElement.setAttribute('id', 'rating');

const options = [
    { value: "4", text: "4 Stars" },
    { value: "3", text: "3 Stars" },
    { value: "2", text: "2 Stars" },
    { value: "1", text: "1 Star" }
];

options.forEach(function(option) {
    let optionElement = document.createElement('option');
    optionElement.setAttribute('value', option.value);
    optionElement.textContent = option.text;
    selectElement.append(optionElement);
});

const mainElement = document.querySelector('main');
const featuresSection = mainElement.querySelector('#Features');
mainElement.insertBefore(selectElement, featuresSection);

selectElement.style.margin = '0 auto';
selectElement.style.display = 'block';

 